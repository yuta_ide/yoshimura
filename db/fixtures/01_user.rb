User.seed do |s|
  s.id = 1
  s.name = "生田"
  s.email = "ikuta@xxx.com"
  s.count = 0
end

User.seed do |s|
  s.id = 2
  s.name = "吉村"
  s.email = "yoo46ra@gmail.com"
  s.count = 0
end

User.seed do |s|
  s.id = 3
  s.name = "小泉"
  s.email = "s1550020@jaist.ac.jp"
  s.count = 0
end

User.seed do |s|
  s.id = 4
  s.name = "井手"
  s.email = "sadness.ojisan@gmail.com"
  s.count = 0
end
