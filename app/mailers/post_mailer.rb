class PostMailer < ApplicationMailer
  default from: "s1550020@jaist.ac.jp"
  def post_email(user)
    logger.debug("[PostMailer post_email] : fire")
    @name = user.name
    @count = user.count
    logger.debug("[PostMailer post_email] user.email: " + user.email.to_s)
    mail to: user.email, subject: "あなたとお話したい人が現れました"
  end
end
